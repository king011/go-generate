# go-generate

golang 實現的 代碼生成工具

go-generate 可以通過模板 爲項目創建 一些代碼 檔案 [go-generate-template](https://gitlab.com/king011/go-generate-template) 項目 提供了一些 代碼模板

# 模板存儲位置

go-generate 會首先在 go-generate所在檔案夾下 的 **templates** 檔案夾下 查找模板

如果沒有找到 則在環境變量 **GoGenerateTemplate** 指定的 路徑中 查找模板