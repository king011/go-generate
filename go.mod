module gitlab.com/king011/go-generate

go 1.12

require (
	github.com/google/go-jsonnet v0.13.0
	github.com/spf13/cobra v0.0.5
)
