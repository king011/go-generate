package main

import (
	"log"

	"gitlab.com/king011/go-generate/cmd"
)

func main() {
	if e := cmd.Execute(); e != nil {
		log.Fatalln(e)
	}
}
