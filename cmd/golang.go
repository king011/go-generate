package cmd

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/king011/go-generate/cmd/internal/golang"
	"gitlab.com/king011/go-generate/template"

	"github.com/spf13/cobra"
)

func init() {
	cmd := &cobra.Command{
		Use:   "golang",
		Short: "generate golang code",
	}
	goInit(cmd)
	rootCmd.AddCommand(cmd)
}
func goInit(parent *cobra.Command) {
	var list bool
	var ctx golang.Context
	cmd := &cobra.Command{
		Use:   "init",
		Short: "init golang project",
		Run: func(cmd *cobra.Command, args []string) {
			if list {
				items, e := template.FindTemplate("golang/init")
				if e != nil {
					log.Fatalln(e)
				}
				for i := 0; i < len(items); i++ {
					fmt.Println(items[i])
				}
			} else {
				e := ctx.Format()
				if e != nil {
					log.Fatalln(e)
				}
				e = ctx.ExecInit()
				if e != nil {
					os.Exit(1)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.BoolVar(&list,
		"list",
		false,
		"list all template",
	)
	flags.StringVarP(&ctx.Package,
		"package", "p",
		"",
		"package name",
	)
	flags.StringVar(&ctx.Project,
		"project",
		"",
		"project name",
	)
	flags.StringVarP(&ctx.Template,
		"template", "t",
		"",
		"template name",
	)
	flags.StringVarP(&ctx.Mode,
		"mode", "m",
		"create",
		"code generate mode [create touch update]",
	)

	parent.AddCommand(cmd)
}
