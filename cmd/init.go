package cmd

import (
	"fmt"
	"log"
	"runtime"

	"gitlab.com/king011/go-generate/version"

	"github.com/spf13/cobra"
)

const (
	// App .
	App = "go-generate"
)

var v bool
var rootCmd = &cobra.Command{
	Use:   App,
	Short: "generate code tools",
	Run: func(cmd *cobra.Command, args []string) {
		if v {
			fmt.Println(runtime.GOOS, runtime.GOARCH, runtime.Version())
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
		} else {
			fmt.Println(App)
			fmt.Println(runtime.GOOS, runtime.GOARCH, runtime.Version())
			fmt.Println(version.Tag)
			fmt.Println(version.Commit)
			fmt.Println(version.Date)
			fmt.Printf(`Use "%v --help" for more information about this program.
`, App)
		}
	},
}

func init() {
	flags := rootCmd.Flags()
	flags.BoolVarP(&v,
		"version",
		"v",
		false,
		"show version",
	)
	log.SetFlags(log.LstdFlags | log.Lshortfile)
}

// Execute run command
func Execute() error {
	return rootCmd.Execute()
}
