package cmd

import (
	"fmt"
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-generate/cmd/internal/cpp"
	"gitlab.com/king011/go-generate/template"
)

func init() {
	cmd := &cobra.Command{
		Use:   "c++",
		Short: "generate c++ code",
	}
	cppInit(cmd)
	rootCmd.AddCommand(cmd)
}
func cppInit(parent *cobra.Command) {
	var list bool
	var ctx cpp.Context
	cmd := &cobra.Command{
		Use:   "init",
		Short: "init c++ project",
		Run: func(cmd *cobra.Command, args []string) {
			if list {
				items, e := template.FindTemplate("c++/init")
				if e != nil {
					log.Fatalln(e)
				}
				for i := 0; i < len(items); i++ {
					fmt.Println(items[i])
				}
			} else {
				e := ctx.Format()
				if e != nil {
					log.Fatalln(e)
				}
				e = ctx.ExecInit()
				if e != nil {
					os.Exit(1)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.BoolVar(&list,
		"list",
		false,
		"list all template",
	)
	flags.StringVarP(&ctx.Project,
		"project", "p",
		"",
		"project name",
	)
	flags.StringVarP(&ctx.Template,
		"template", "t",
		"",
		"template name",
	)
	flags.StringVarP(&ctx.Mode,
		"mode", "m",
		"create",
		"code generate mode [create touch update]",
	)

	parent.AddCommand(cmd)
}
