package completion

import (
	"bufio"
	"log"
)

// ContextCpp context for c++
type ContextCpp struct {
}

// Write 寫入 bash completion
func (c *ContextCpp) Write(w *bufio.Writer) (e error) {
	e = c.init(w)
	if e != nil {
		return
	}

	e = c.base(w)
	if e != nil {
		return
	}
	return
}
func (c *ContextCpp) init(w *bufio.Writer) (e error) {
	_, e = w.WriteString(`function __king011_generate_cpp_init(){
	local opts='-h --help \
	-p --project -t --template -m --mode \
	--list'
	case ${COMP_WORDS[COMP_CWORD-1]} in
		-p|--project)
			COMPREPLY=( )
		;;
		-t|--template)
			opts=$(go-generate c++ init --list)
			COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
		;;
		-m|--mode)
			opts="create touch update"
			COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
		;;
		*)
			COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
		;;
	esac
}
`)
	if e != nil {
		log.Fatalln(e)
	}
	return
}

func (c *ContextCpp) base(w *bufio.Writer) (e error) {
	_, e = w.WriteString(`function __king011_go_generate_cpp(){
	if [ 2 == $COMP_CWORD ];then
		local opts='-h --help \
			init'
		COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	else
		case ${COMP_WORDS[2]} in
			init)
				__king011_generate_cpp_init
			;;
		esac
	fi
}
`)
	if e != nil {
		log.Fatalln(e)
	}
	return
}
