package completion

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"time"
)

// Context 執行環境
type Context struct {
	bashCompletion ContextBashCompletion
	golang         ContextGolang
	cpp            ContextCpp
}

// Write 寫入 bash completion
func (c *Context) Write(writer io.Writer) (e error) {
	w := bufio.NewWriter(writer)
	e = c.begin(w)
	if e != nil {
		return
	}
	e = c.bashCompletion.Write(w)
	if e != nil {
		return
	}
	e = c.golang.Write(w)
	if e != nil {
		return
	}
	e = c.cpp.Write(w)
	if e != nil {
		return
	}
	e = c.end(w)
	if e != nil {
		return
	}
	w.Flush()
	return
}

func (c *Context) begin(w *bufio.Writer) (e error) {
	_, e = w.WriteString(
		fmt.Sprintf(`#!/bin/bash
#Program:
#       go-generate for bash completion
#History:
#       %s create by go-generate
#Email:
#       zuiwuchang@gmail.com
`,
			time.Now().Format("2006-01-02 15:04:05"),
		),
	)
	if e != nil {
		log.Fatalln(e)
	}
	return
}

func (c *Context) end(w *bufio.Writer) (e error) {
	_, e = w.WriteString(`function __king011_go_generate(){
	local cur=${COMP_WORDS[COMP_CWORD]}
	if [ 1 == $COMP_CWORD ];then
		local opts='-h --help -v --version \
			bash-completion \
			golang c++ \
			help'
		COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
	else
		case ${COMP_WORDS[1]} in
			bash-completion)
				__king011_go_generate_bash_completion
			;;
			golang)
				__king011_go_generate_golang
			;;
			c++)
				__king011_go_generate_cpp
			;;
		esac
	fi
}
complete -F __king011_go_generate go-generate
complete -F __king011_go_generate go-generate.exe
`)
	if e != nil {
		log.Fatalln(e)
	}
	return
}
