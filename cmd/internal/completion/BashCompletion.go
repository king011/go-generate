package completion

import (
	"bufio"
	"log"
)

// ContextBashCompletion context for bash completion
type ContextBashCompletion struct {
}

// Write 寫入 bash completion
func (c *ContextBashCompletion) Write(w *bufio.Writer) (e error) {
	_, e = w.WriteString(`function __king011_go_generate_bash_completion(){
	local opts='-h --help \
	-o --output'
	case ${COMP_WORDS[COMP_CWORD-1]} in
		-o|--output)
			_filedir || COMPREPLY=( $(compgen -o plusdirs -f ${cur}) )
		;;

		*)
			COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
		;;
	esac
}
`)
	if e != nil {
		log.Fatalln(e)
	}
	return
}
