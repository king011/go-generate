package golang

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"time"

	"gitlab.com/king011/go-generate/template"
)

// Context 環境定義
type Context struct {
	// 包名
	Package string
	// 項目名稱
	Project string
	// 代碼生成模式
	Mode string
	// 模板
	Template string

	// 創建時間
	Now time.Time
}

// Format 格式化 設置
func (c *Context) Format() (e error) {
	c.Package = strings.TrimSpace(c.Package)
	c.Project = strings.TrimSpace(c.Project)
	c.Mode = strings.ToLower(strings.TrimSpace(c.Mode))
	c.Template = strings.TrimSpace(c.Template)
	if c.Mode != "create" && c.Mode != "touch" && c.Mode != "update" {
		e = fmt.Errorf("not support mode : %s", c.Mode)
		return
	}
	if c.Template == "" {
		e = errors.New("template not defined")
		return
	}
	for strings.HasPrefix(c.Package, "/") {
		c.Package = c.Package[1:]
	}
	for strings.HasSuffix(c.Package, "/") {
		c.Package = c.Package[:len(c.Package)-1]
	}
	if c.Package == "" {
		c.Package, e = c.readPackage()
		if e != nil {
			return
		}
		if c.Package == "" {
			e = errors.New("package not defined")
			return
		}
	}
	if c.Project == "" {
		find := strings.LastIndex(c.Package, "/")
		if find == -1 {
			c.Project = c.Package
		} else {
			c.Project = c.Package[find+1:]
		}
	}
	c.Now = time.Now()
	return
}

func (c *Context) readPackage() (pkg string, e error) {
	f, e := os.Open("go.mod")
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}
	defer f.Close()
	r := bufio.NewReader(f)
	var b []byte
	for {
		b, _, e = r.ReadLine()
		if e == io.EOF {
			e = nil
			break
		}
		str := strings.TrimSpace(string(b))
		if strings.HasPrefix(str, "module") {
			str = str[len("module"):]
			pkg = strings.TrimSpace(str)
			break
		}
	}
	return
}

// ExecInit 執行 初始化
func (c *Context) ExecInit() (e error) {
	ctx := template.Context{
		Mode: c.Mode,
		Data: c,
	}
	e = ctx.Reader("golang/init", c.Template)
	if e != nil {
		log.Println(e)
	}
	return
}
