package cpp

import (
	"errors"
	"fmt"
	"log"
	"strings"
	"time"

	"gitlab.com/king011/go-generate/template"
)

// Context 環境定義
type Context struct {
	// 項目名稱
	Project string
	// 代碼生成模式
	Mode string
	// 模板
	Template string

	// 創建時間
	Now time.Time
}

// Format 格式化 設置
func (c *Context) Format() (e error) {
	c.Project = strings.TrimSpace(c.Project)
	c.Mode = strings.ToLower(strings.TrimSpace(c.Mode))
	c.Template = strings.TrimSpace(c.Template)
	if c.Mode != "create" && c.Mode != "touch" && c.Mode != "update" {
		e = fmt.Errorf("not support mode : %s", c.Mode)
		return
	}
	if c.Template == "" {
		e = errors.New("template not defined")
		return
	}

	c.Now = time.Now()
	return
}

// ExecInit 執行 初始化
func (c *Context) ExecInit() (e error) {
	if c.Project == "" {
		e = errors.New("project not defined")
		return
	}

	ctx := template.Context{
		Mode: c.Mode,
		Data: c,
	}
	e = ctx.Reader("c++/init", c.Template)
	if e != nil {
		log.Println(e)
	}
	return
}
