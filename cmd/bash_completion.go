package cmd

import (
	"log"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/king011/go-generate/cmd/internal/completion"
)

func init() {
	var output string
	cmd := &cobra.Command{
		Use:   "bash-completion",
		Short: "create bash-completion scripts",
		Run: func(cmd *cobra.Command, args []string) {
			var ctx completion.Context
			if output == "" {
				e := ctx.Write(os.Stdout)
				if e != nil {
					log.Fatalln(e)
				}
			} else {
				f, e := os.Create(output)
				if e != nil {
					log.Fatalln(e)
				}
				e = ctx.Write(f)
				f.Close()
				if e != nil {
					log.Fatalln(e)
				}
			}
		},
	}
	flags := cmd.Flags()
	flags.StringVarP(&output,
		"output", "o",
		"",
		"output file,if empty output stdout",
	)
	rootCmd.AddCommand(cmd)
}
