package template

import (
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"gitlab.com/king011/go-generate/utils"
)

// Env 環境變量名
const Env = "GoGenerateTemplate"

// GetTemplate 返回最先找到的模板
func GetTemplate(group, name string) (root string) {
	str := utils.BasePath() + "/templates/" + group + "/" + name
	if isTemplate(str) {
		root = filepath.Clean(str)
		return
	}
	var sep string
	if runtime.GOOS == "windows" {
		sep = ";"
	} else {
		sep = ":"
	}
	str = strings.TrimSpace(os.Getenv(Env))
	if str == "" {
		return
	}
	strs := strings.Split(str, sep)
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		str += "/" + group + "/" + name
		if isTemplate(str) {
			root = filepath.Clean(str)
			return
		}
	}
	return
}
func isTemplate(filapath string) (ok bool) {
	f, e := os.Open(filapath)
	if e != nil {
		if !os.IsNotExist(e) {
			log.Println(e)
		}
		return
	}
	info, e := f.Stat()
	f.Close()
	if e != nil {
		log.Println(e)
		return
	}
	ok = info.IsDir()
	return
}
