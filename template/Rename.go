package template

import "regexp"

// Rename 模板 路徑改名
type Rename struct {
	From    string
	Replace string
}

// RenameMatch 改名 匹配模式
type RenameMatch struct {
	From    *regexp.Regexp
	Replace string
}

// NewRenameMatch .
func NewRenameMatch(from, replace string) (m RenameMatch, e error) {
	match, e := regexp.Compile(from)
	if e != nil {
		return
	}
	m.From = match
	m.Replace = replace
	return
}
