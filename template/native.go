package template

import (
	"time"
)

// 格式 時間
func nativeFormatTime(t time.Time, layout string) string {
	return t.Format(layout)
}
