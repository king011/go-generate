package template

import (
	"log"
	"os"
	"runtime"
	"strings"

	"gitlab.com/king011/go-generate/utils"
)

// FindTemplate 查找 模板
func FindTemplate(group string) (templates []string, e error) {
	keys := make(map[string]bool)
	templates = findTemplate(utils.BasePath()+"/templates/"+group, keys, templates)

	var str, sep string
	if runtime.GOOS == "windows" {
		sep = ";"
	} else {
		sep = ":"
	}
	str = strings.TrimSpace(os.Getenv(Env))
	if str == "" {
		return
	}
	strs := strings.Split(str, sep)
	for _, str := range strs {
		str = strings.TrimSpace(str)
		if str == "" {
			continue
		}
		templates = findTemplate(str+"/"+group, keys, templates)
	}
	return
}
func findTemplate(root string, keys map[string]bool, templates []string) []string {
	f, e := os.Open(root)
	if e != nil {
		if !os.IsNotExist(e) {
			log.Println(e)
		}
		return templates
	}
	defer f.Close()
	info, e := f.Stat()
	if e != nil {
		log.Println(e)
		return templates
	}
	if !info.IsDir() {
		return templates
	}
	items, e := f.Readdir(0)
	if e != nil {
		return templates
	}
	for i := 0; i < len(items); i++ {
		info = items[i]
		if !info.IsDir() {
			continue
		}
		name := info.Name()
		if strings.HasPrefix(name, ",") || keys[name] {
			continue
		}
		keys[name] = true
		templates = append(templates, name)
	}
	return templates
}
