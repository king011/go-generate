package template

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"github.com/google/go-jsonnet"
)

const _RenameFile = "__rename.jsonnet"
const _ConfigureFile = "__configure.jsonnet"

// Context 渲染上下文
type Context struct {
	Mode   string
	rename []RenameMatch
	binary []*regexp.Regexp
	Data   interface{}
}

func (c *Context) initRename(root string) (e error) {
	b, e := ioutil.ReadFile(root + "/" + _RenameFile)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}

	var items []Rename
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	t := template.New(_RenameFile)
	expendTemplate(t)
	t, e = t.Parse(jsonStr)
	if e != nil {
		return
	}
	var w bytes.Buffer
	e = t.Execute(&w, c.Data)
	if e != nil {
		return
	}
	e = json.Unmarshal(w.Bytes(), &items)
	if e != nil {
		return
	}
	var item RenameMatch
	for i := 0; i < len(items); i++ {
		if items[i].From == "" {
			continue
		}
		item, e = NewRenameMatch(items[i].From, items[i].Replace)
		if e != nil {
			return
		}
		c.rename = append(c.rename, item)
	}
	return
}
func (c *Context) initConfigure(root string) (e error) {
	b, e := ioutil.ReadFile(root + "/" + _ConfigureFile)
	if e != nil {
		if os.IsNotExist(e) {
			e = nil
		}
		return
	}

	var obj struct {
		Binary []string
	}
	vm := jsonnet.MakeVM()
	var jsonStr string
	jsonStr, e = vm.EvaluateSnippet("", string(b))
	if e != nil {
		return
	}
	t := template.New(_ConfigureFile)
	expendTemplate(t)
	t, e = t.Parse(jsonStr)
	if e != nil {
		return
	}
	var w bytes.Buffer
	e = t.Execute(&w, c.Data)
	if e != nil {
		return
	}
	e = json.Unmarshal(w.Bytes(), &obj)
	if e != nil {
		return
	}

	for _, str := range obj.Binary {
		if str == "" {
			continue
		}
		var match *regexp.Regexp
		match, e = regexp.Compile(str)
		if e != nil {
			return
		}
		c.binary = append(c.binary, match)
	}
	return
}

// Reader 渲染 模板
func (c *Context) Reader(group, name string) (e error) {
	// 查找 模板
	root := GetTemplate(group, name)
	if root == "" {
		e = fmt.Errorf("not found template : %s", name)
		return
	}
	e = c.initRename(root)
	if e != nil {
		return
	}
	e = c.initConfigure(root)
	if e != nil {
		return
	}

	e = filepath.Walk(root, func(path string, info os.FileInfo, err error) (e error) {
		if err != nil {
			e = err
			log.Println(e)
			return
		}
		if len(path) <= len(root)+1 {
			return
		}
		name := path[len(root)+1:]
		if name == _RenameFile || name == _ConfigureFile {
			return
		}
		// 拷貝
		name = strings.ReplaceAll(name, `\`, `/`)
		var copied bool
		for i := 0; i < len(c.binary); i++ {
			item := c.binary[i]
			if item.MatchString(name) {
				copied = true
				break
			}
		}
		// 改名
		for i := 0; i < len(c.rename); i++ {
			item := c.rename[i]
			if item.From.MatchString(name) {
				name = item.From.ReplaceAllString(name, item.Replace)
				break
			}
		}
		e = c.reader(path, name, c.Mode, c.Data, copied)
		if e != nil {
			log.Println(e)
		}
		return
	})
	return
}

func (c *Context) reader(filename, dst, mode string, data interface{}, copied bool) (e error) {
	f, e := os.Open(filename)
	if e != nil {
		return
	}
	defer f.Close()
	info, e := f.Stat()
	if e != nil {
		return
	}
	if info.IsDir() {
		e = c.readerDirectory(info.Mode(), dst, mode)
	} else {
		e = c.readerFile(f, info.Mode(), dst, mode, data, copied)
	}
	return
}
func (c *Context) readerDirectory(perm os.FileMode, dst, mode string) (e error) {
	f, e := os.Open(dst)
	if e == nil {
		var info os.FileInfo
		info, e = f.Stat()
		f.Close()
		if e != nil {
			return
		}
		if !info.IsDir() {
			e = fmt.Errorf("Expected folder but file already exists : %s", dst)
			return
		}
	} else {
		if os.IsNotExist(e) {
			e = os.MkdirAll(dst, perm)
		}
		return
	}
	return
}
func (c *Context) readerFile(r *os.File, perm os.FileMode, dst, mode string, data interface{}, copied bool) (e error) {
	var w *os.File
	switch mode {
	case "create":
		var f *os.File
		f, e = os.Open(dst)
		if e == nil {
			var info os.FileInfo
			info, e = f.Stat()
			f.Close()
			if e != nil {
				return
			}
			if info.IsDir() {
				e = fmt.Errorf("Expected file but folder already exists : %s", dst)
			}
			return
		} else if !os.IsNotExist(e) {
			return
		}
		w, e = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, perm)
		if e != nil {
			return
		}
	case "touch":
		w, e = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, perm)
		if e != nil {
			return
		}
	case "update":
		var f *os.File
		f, e = os.Open(dst)
		if e == nil {
			var info os.FileInfo
			info, e = f.Stat()
			f.Close()
			if e != nil {
				return
			}
			if info.IsDir() {
				e = fmt.Errorf("Expected file but folder already exists : %s", dst)
				return
			}
			w, e = os.OpenFile(dst, os.O_RDWR|os.O_CREATE|os.O_TRUNC, perm)
			if e != nil {
				return
			}
		} else {
			if os.IsNotExist(e) {
				e = nil
			}
			return
		}
	default:
		e = fmt.Errorf("not support mode : %s", mode)
		return
	}
	defer func() {
		w.Close()
		if e != nil {
			os.Remove(dst)
		}
	}()
	if copied {
		_, e = io.Copy(w, r)
		if e != nil {
			return
		}
	} else {
		var b []byte
		b, e = ioutil.ReadAll(r)
		if e != nil {
			return
		}
		t := template.New(dst)
		expendTemplate(t)
		t, e = t.Parse(string(b))
		if e != nil {
			return
		}
		e = t.Execute(w, data)
	}
	return
}
func expendTemplate(t *template.Template) {
	t.Funcs(template.FuncMap{
		"FormatTime": nativeFormatTime,
	})
}
